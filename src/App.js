import React, { Component } from "react";
import staticUsers from "../src/data";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      staticUsers: staticUsers,
    };
  }

  render() {
    const { staticUsers } = this.state;

    return (
      <div className="App container">
        <ul>
          {staticUsers.map((user) => (
            <li key={`static-user-${user.id}`}>
              <h1>{user.name}</h1>
              <img src={user.avatar_url} alt={user.name} />
            </li>
          ))}
        </ul>
      </div>
    );
  }
}
export default App;

// components:
// User
// AddUser
// ListUsers
